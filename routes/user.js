const express = require("express")
	  router = express.Router()
	  userController = require("../controllers/user")
	  auth = require("../auth")

// Router checking if user's email already exists in the database

router.post("/checkEmail", (req, res) => {
	userController.checkEmailExists(req.body).then(resultFromController => res.send(resultFromController))
})

// Route for registering a user

router.post("/register", (req, res) => {
	userController.registerUser(req.body).then(resultFromController => res.send(resultFromController))
})

// Route for user authentication
router.post("/login", (req, res) => {
	userController.loginUser(req.body).then(resultFromController => res.send(resultFromController))
})

// Activity
router.get("/details", auth.verify, (req, res) => {
	auth.decode(req.headers.authorization)

	userController.getProfile(req.body).then(resultFromController => res.send(resultFromController))
})

// Route to enroll user to a course
router.post("/enroll", auth.verify, (req, res) => {
	let data = {
		userId : req.body.userId,
		courseId : req.body.courseId
	}

	const token = auth.decode(req.headers.authorization).userId
	if(data.userId == token) {
	userController.enroll(data).then(resultFromController => res.send(resultFromController))
	}
	else {
		res.send(`Cannot enroll.`)
	}
})

module.exports = router