const express = require("express")
	  router = express.Router()
	  courseController = require("../controllers/course")
	  auth = require("../auth")

// Course for creating a course
router.post("/", auth.verify, (req, res) => {
	const reqBody = {
		course : req.body,
		isAdmin : auth.decode(req.headers.authorization).isAdmin
	}

	if(reqBody.isAdmin == true) {
	courseController.addCourse(reqBody).then(resultFromController => res.send(resultFromController))
	}
	else {
		res.send(`Cannot add course. User is not an admin.`)
	}
})

// Route for retrieving all the courses
router.get("/all", (req, res) => {
	courseController.getAllCourses().then(resultFromController => res.send(resultFromController))
})

// Route for retrieving specific course
router.get("/:courseId", (req, res) => {
	console.log(req.params.courseId)

	courseController.getCourse(req.params).then(resultFromController => res.send(resultFromController))
})

// Route for updating a course
router.put("/:courseId", auth.verify, (req, res) => {
	const verified = {
		isAdmin : auth.decode(req.headers.authorization).isAdmin
	}

	if(verified.isAdmin == true) {
	courseController.updateCourse(req.params, req.body).then(resultFromController => res.send(resultFromController))
	}
	else {
		res.send(`Cannot update course. User is not an admin.`)
	}
})

// Activity
router.put("/:courseId/archive", auth.verify, (req, res) => {
	const verified = {
		isAdmin : auth.decode(req.headers.authorization).isAdmin
	}

	if(verified.isAdmin == true) {
	courseController.archiveCourse(req.params, req.body).then(resultFromController => res.send(resultFromController))
	}
	else {
		res.send(`Cannot archive course. User is not an admin.`)
	}
})

module.exports = router