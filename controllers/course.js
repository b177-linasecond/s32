const Course = require("../models/Course")

module.exports.addCourse = (reqBody) => {
	// Create a variable "newCourse" and instantiates a new "Course" object
	let newCourse = new Course({
		name : reqBody.course.name,
		description : reqBody.course.description,
		price : reqBody.course.price
	})

	return newCourse.save().then((course, error) => {
		if(error) {
			return false
		}
		else {
			return true
		}
	})
}

// Controller function for getting all courses
module.exports.getAllCourses = () => {
	return Course.find({}).then(result => result)
}

// Controller function for retrieving a specific course
module.exports.getCourse = (reqParams) => {
	return Course.findById(reqParams.courseId).then(result => result)
}

// Controller function for updating a course
module.exports.updateCourse = (reqParams, reqBody) => {
	// Specify the fields/properties of the document to be updated
	let updatedCourse = {
		name : reqBody.name,
		description : reqBody.description,
		price : reqBody.price
	}

	return Course.findByIdAndUpdate(reqParams.courseId, updatedCourse).then((course, error) => {
		if(error) {
			return false
		}
		else {
			return true
		}
	})
}

module.exports.archiveCourse = (reqParams, reqBody) => {
	let updatedCourse = {
		isActive : false
	}

	return Course.findByIdAndUpdate(reqParams.courseId, updatedCourse).then((course, error) => {
		if(error) {
			return false
		}
		else {
			return true
		}
	})
}