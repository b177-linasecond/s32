// Imports the User model

const User = require("../models/User")
	  Course = require("../models/Course")
	  bcrypt = require("bcrypt")
	  auth = require("../auth")

// Controller function for checking email duplicates

module.exports.checkEmailExists = (reqBody) => {
	return User.find({email : reqBody.email}).then(result => {
		if(result.length > 0) {
			return result
		}
		else {
			return false
		}
	})
}

// Controller function for user registration

module.exports.registerUser = (reqBody) => {
	// Creates variable "newUser" and instantiates a new "User" object using the mongoose model
	let newUser = new User({
		firstName : reqBody.firstName,
		lastName : reqBody.lastName,
		email : reqBody.email,
		password : bcrypt.hashSync(reqBody.password, 10),
		mobileNo : reqBody.mobileNo
	})

	// Saves the created object to our database
	return newUser.save().then((user, error) => {
		// User registration failed
		if(error) {
			return false
		}

		// User registration successful
		else {
			return true
		}
	})
}

// User authentication (login)

module.exports.loginUser = (reqBody) => {
	return User.findOne({email : reqBody.email}).then(result => {
		// User does not exist
		if(result == null) {
			return false
		}
		// User exists
		else {
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password)

			if(isPasswordCorrect) {
				// Generate an access token
				return {access : auth.createAccessToken(result)}
			}
			// Password do not match
			else {
				return false
			}
		}
	})
}

// Activity

module.exports.getProfile = (reqBody) => {
	return User.findOne({_id : reqBody._id}).then((result) => {
		if(result == null) {
			return false
		}
		else {
			result.password = ""
			return result
		}
	})
}


// Controller for enrolling the user for a specific course
module.exports.enroll = async (data) => {
	let isUserUpdated = await User.findById(data.userId).then(user => {
		// Add the courseId in the user's enrollments array
		user.enrollments.push({courseId : data.courseId})

		// Save the updated user information in the database
		return user.save().then((user, error) => {
			if(error) {
				return false
			}
			else {
				return true
			}
		})
	})
	// Add the user ID in the enrollees array of the course
	let isCourseUpdated = await Course.findById(data.courseId).then(course => {
		course.enrollees.push({userId : data.userId})

		return course.save().then((course, error) => {
			if(error) {
				return false
			}
			else {
				return true
			}
		})
	})

	// Condition that will check if the user and course documents have been updated
	// User enrollment is successful
	if(isUserUpdated && isCourseUpdated) {
		return true
	}
	else {
		return false
	}

}