const express = require("express")
	  mongoose = require("mongoose")

	  // Allows our back-end application to be available to our front-end application

	  cors = require("cors")
	  app = express()

	  // Allows access to routes defined within the application

	  userRoute = require("./routes/user")
	  courseRoute = require("./routes/course")

// Connect to our MongoDB database

mongoose.connect("mongodb+srv://admin:admin@wdc028-course-booking.fgiy7.mongodb.net/S32-S36?retryWrites=true&w=majority",{
		useNewUrlParser : true,
		useUnifiedTopology : true
})

// Prompts a message for successful database connection

mongoose.connection.once("open", () => console.log("Now connected to MongoDB Atlas"))

app.use(express.json())
app.use(express.urlencoded({extended : true}))

// Allow all resources to access the back-end application

app.use(cors())

// Defines the "/users" string to be included for all user routes defined in the "user" route file

app.use("/users", userRoute)

// Defines the "/courses" string to be included for all course routes defined in the "course" route file

app.use("/courses", courseRoute)

// App listening to port 4000

app.listen(process.env.PORT || 4000, () => console.log(`API is now online on port ${process.env.PORT || 4000}`))